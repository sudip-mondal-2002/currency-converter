package com.sudip.currencyconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public  void converter (View view){
        EditText rupeesText = (EditText) findViewById(R.id.rupees);
        double rupeesValue = Double.parseDouble(rupeesText.getText().toString());
        double usdValue = rupeesValue*0.013;
        String usdText = "USD value : "+ String.format("%.2f",usdValue);
        Toast.makeText(this, usdText, Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}